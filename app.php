<?php

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
require __DIR__ . '\src\chatBot.php';
// Main route
$app->match('/', function (Application $app, Request $request) {
    //$app['session']->clear();
    //process the form calling classes
    $bot = new chatBot();
    $response = $bot->chatQuestions($app, $request->get('message'));

    return $app['twig']->render('index.html', array(
        'title' => 'Bot exercise',
        'name' => 'Paloma',
        'message' => $response
    ));
});

$app->match('/chat', function (Application $app, Request $request) {
    //process the form calling classes
    $bot = new chatBot();
    $response = $bot->chatQuestions($app, $request->get('message'));

    return $app['twig']->render('index.html', array(
        'title' => 'Bot exercise',
        'name' => 'Paloma',
        'message' => $response
    ));
});