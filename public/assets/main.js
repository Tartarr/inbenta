$(function() {
    $(".bot__messages").animate({ scrollTop: $('.bot__messages').prop("scrollHeight")}, 1000);

    $('#message').keypress(function(e){
        if(e.which == 13){//Enter key pressed
            sendAjax();
        }
    });
    $('#submit').click(function() {
        sendAjax();
    });
});

function sendAjax(){
    $.ajax({
        url: "/public/index.php/chat",
        type: "POST",
        data: {
            message:$('#message').val(),
        },
        success: function(data)
        {
            var $div = $(data).find("div.bot__messages");
            var html = $div.html();
            $('.bot__messages').html(html);
            $(".bot__messages").animate({ scrollTop: $('.bot__messages').prop("scrollHeight")}, 1000);
            $('#message').val('');
        }
    });
}