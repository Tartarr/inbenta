<?php
/**
 * Created by PhpStorm.
 * User: Tartar
 * Date: 01/11/2017
 * Time: 10:33
 */

class chatBot
{
    private $questions = array(
        'name' => 'What is your name?',
        'email' => 'What is your email?',
        'age' => 'How old are you?'
    );

    function chatQuestions($app, $message){
        $response = array();
        if ($app['session']->get('start') === null){
            $app['session']->set('start', 1);
            //devolvemos array respuesta - hola + 1st question
            $response = $this->prepareMessage($app, $message, 'hello');
            return $response;
        }
        if ($message != null){
            if ($app['session']->get('name') === null) {
                $app['session']->set('name', $message);
                $response = $this->prepareMessage($app, $message, 'name');
            }elseif($app['session']->get('email') === null){
                if (filter_var($message, FILTER_VALIDATE_EMAIL)) {
                    $app['session']->set('email', $message);
                    $response = $this->prepareMessage($app, $message, 'email');
                }else{
                    $response = $this->prepareMessage($app, $message, 'email_error');
                }
            }elseif($app['session']->get('age') === null){
                if (preg_match("/^[0-9]+$/", $message)) {
                    $app['session']->set('age', $message);
                    $response = $this->prepareMessage($app, $message, 'age');
                }else{
                    $response = $this->prepareMessage($app, $message, 'age_error');
                }
            }else{
                $response = $app['session']->get('conversation');
            }
        }else{
            $response = $app['session']->get('conversation');
        }
        return $response;
    }

    function prepareMessage($app, $msg, $key){
        $response = $app['session']->get('conversation');
        switch ($key) {
            case 'hello':
                $response[] = array('bot' => 'Hello! I will ask you some questions ok?');
                $response[] = array('bot' => $this->questions['name']);
                break;
            case 'name':
                $response[] = array('user' => $msg);
                $response[] = array('bot' => $this->questions['email']);
                break;
            case 'email':
                $response[] = array('user' => $msg);
                $response[] = array('bot' => $this->questions['age']);
                break;
            case 'email_error';
                $response[] = array('user' => $msg);
                $response[] = array('bot' => 'Sorry, I could not understand your email address');
                $response[] = array('bot' => $this->questions['email']);
                break;
            case 'age':
                $response[] = array('user' => $msg);
                $response[] = array('bot' => "Thanks! Now I know you better. Your name is {$app['session']->get('name')}, you are {$app['session']->get('age')} old and I can contact you on {$app['session']->get('email')}");
                break;
            case 'age_error';
                $response[] = array('user' => $msg);
                $response[] = array('bot' => 'Sorry, I could not understand your age');
                $response[] = array('bot' => $this->questions['age']);
                break;
        }
        $app['session']->set('conversation', $response);
        return $app['session']->get('conversation');
    }
}